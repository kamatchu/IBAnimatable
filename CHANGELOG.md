# Next

### API breaking changes
- Remove `DesignableGradientView`, use `AnimatableView` to configure gradient. [#81](https://github.com/JakeLin/IBAnimatable/issues/81) and [#86](https://github.com/JakeLin/IBAnimatable/issues/86)

### Enhancements

- New animations: Rotate, RotateCCW [#51](https://github.com/JakeLin/IBAnimatable/issues/51)
- New masks: Star, Triangle, Polygon, Wave
- SidebarImage now support right images 
- Predefined gradients [#24](https://github.com/JakeLin/IBAnimatable/issues/24)

### Bugfixes

- Fixed left gradient
- Fixed slideOut / slideIn animations [#46](https://github.com/JakeLin/IBAnimatable/issues/46)

# 1.0.1

### Enhancements

- Carthage support - [#6](https://github.com/JakeLin/IBAnimatable/issues/6)

# 1.0

- Initial release